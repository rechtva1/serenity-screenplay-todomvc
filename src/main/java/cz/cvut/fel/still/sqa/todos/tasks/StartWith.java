package cz.cvut.fel.still.sqa.todos.tasks;

import cz.cvut.fel.still.sqa.todos.pageobjects.ApplicationHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

/**
 * @author Vaclav Rechtberger
 */
public class StartWith implements Task {
    ApplicationHomePage applicationHomePage;
    @Override
    @Step("{0} starts with an empty todo list")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(applicationHomePage)
        );
    }

    public static StartWith anEmptyTodoList() {
        return instrumented(StartWith.class);
    }
}