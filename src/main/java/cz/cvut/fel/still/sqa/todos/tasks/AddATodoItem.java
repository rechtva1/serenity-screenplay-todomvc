package cz.cvut.fel.still.sqa.todos.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;

import static cz.cvut.fel.still.sqa.todos.pageobjects.TodoList.WHAT_NEEDS_TO_BE_DONE;
import static net.serenitybdd.screenplay.Tasks.instrumented;

/**
 * @author Vaclav Rechtberger
 */
public class AddATodoItem implements Task {
    private String item;

    public AddATodoItem(String item) {
        this.item = item;
    }

    @Step("{0} adds an item called '#item'")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(item).into(WHAT_NEEDS_TO_BE_DONE).thenHit(Keys.ENTER)
        );
    }

    public static AddATodoItem called(String item) {
        return instrumented(AddATodoItem.class, item);
    }
}
